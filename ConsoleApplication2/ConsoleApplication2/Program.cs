﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Practica3
    {
        static void Main(string[] args)
        {

            //ejercicio1();
            //ejercicio2();
            //ejercicio3();
            //ejercicio4();
            //ejercicio5();
            //ejercicio5a();
            //ejercicio6();
            //ejercicio6a();
            //ejercicio7();
            ejercicio8();
            //ejercicio10();
        }


        public static void ejercicio1()
        {

            int sumi = 0;
            int sump = 0;

            for (int i = 2; i >= 2 && i <= 499; i++)
            {
                if (i % 2 == 0)
                {
                    sump = sump + i;
                }
                if (i % 2 == 1)
                {
                    sumi = sumi + i;
                }



            }
            Console.WriteLine("La suma de pares es " + sump);
            Console.WriteLine("La suma de impares es " + sumi);
            Console.ReadKey();

        }

        public static void ejercicio2()
        {

            for (int i = 1; i >= 1 && i <= 100; i++)
            {
                if (!(i % 3 == 0 || i % 5 == 0))
                {
                    Console.WriteLine("Los numeros son: " + i);
                }



            }

            Console.ReadKey();

        }

        public static void ejercicio3()
        {
            double nota;
            Console.WriteLine("Introduzca su nota: ");
            nota = double.Parse(Console.ReadLine());
            

            if (nota >= 0 && nota <= 10)
            {
                Console.WriteLine($"Su nota es: {nota}");
                if (nota < 5)
                {
                    Console.WriteLine("Suspenso");
                }
                else if (5 <= nota && nota < 7)
                {
                    Console.WriteLine("Aprobado");
                }
                else if (7 <= nota && nota < 9)
                {
                    Console.WriteLine("Notable");
                } else if (9 <= nota && nota <= 10)
                {
                    Console.WriteLine("Sobresaliente");
                }

            }



        }

        public static void ejercicio4()
        {
            int fecha;
            Console.WriteLine("Introduzca el año a determinar");
            fecha = int.Parse(Console.ReadLine());
            if (fecha % 4 == 0 && (fecha % 100 != 0 || fecha % 400 == 0))
            {
                Console.WriteLine($"El año {fecha} es bisiesto");
            }
            else
            {
                Console.WriteLine($"El año {fecha} no es bisiesto");
            }

        }

        public static void ejercicio5()
        {
            int cont = 0; int dado = 0;
            Random r = new Random();
            for (int i = 0; i <= 49; i++)
            {
                dado = r.Next(1, 7);
                if (dado == 1)
                {
                    cont = cont + 1;
                    Console.WriteLine($"{dado}");
                }else
                {
                    Console.WriteLine($"{dado}");
                }
                
            } Console.WriteLine($"El numero 1 ha salido {cont} veces en los 50 lanzamientos");

        }

        public static void ejercicio5a()
        {
            int cont1 = 0; int cont2 = 0 ; int cont3 = 0; int cont4 = 0; int cont5 = 0; int cont6 = 0; int dado = 0;
            Random r = new Random();
            for (int i = 0; i <= 49; i++)
            {
                dado = r.Next(1, 7);
                if (dado == 1)
                {
                    cont1 = cont1 + 1;
                    Console.WriteLine($"{dado}");
                }
                else if (dado == 2)
                {
                    cont2 = cont2 + 1;
                    Console.WriteLine($"{dado}");
                }
                else if (dado == 3)
                {
                    cont3 = cont3 + 1;
                    Console.WriteLine($"{dado}");
                }
                else if (dado == 4)
                {
                    cont4 = cont4 + 1;
                    Console.WriteLine($"{dado}");
                }
                else if (dado == 5)
                {
                    cont5 = cont5 + 1;
                    Console.WriteLine($"{dado}");
                }
                else if (dado == 6)
                {
                    cont6 = cont6 + 1;
                    Console.WriteLine($"{dado}");
                }

            }
            Console.WriteLine($"El numero 1 ha salido {cont1} veces en los 50 lanzamientos");
            Console.WriteLine($"El numero 2 ha salido {cont2} veces en los 50 lanzamientos");
            Console.WriteLine($"El numero 3 ha salido {cont3} veces en los 50 lanzamientos");
            Console.WriteLine($"El numero 4 ha salido {cont4} veces en los 50 lanzamientos");
            Console.WriteLine($"El numero 5 ha salido {cont5} veces en los 50 lanzamientos");
            Console.WriteLine($"El numero 6 ha salido {cont6} veces en los 50 lanzamientos");
        }

        public static void ejercicio6()
        {
            Random r = new Random();

            int longitud;
            Console.WriteLine("Introduzca la cantidad de numeros del array: ");
            longitud = int.Parse(Console.ReadLine());

            int[] miarray = new int[longitud];

            for (int i = 0; i<longitud; i++)
            {
                miarray[i] = r.Next(1, 10);
                Console.WriteLine($"Posicion {i+1} del array: {miarray[i]}"); 
            }
            
        }
        public static void ejercicio6a()
        {
            Random r = new Random();

            int longitud;
            Console.WriteLine("Introduzca la cantidad de numeros del array: ");
            longitud = int.Parse(Console.ReadLine());

            int[] miarray = new int[longitud];
            int min, max;

            for (int i = 0; i < longitud; i++)
            {
                miarray[i] = r.Next(1, 10);
                Console.WriteLine($"Posicion {i + 1} del array: {miarray[i]}");
            }


            max = miarray[0];
            min = miarray[0];
            for(int i =1; i< longitud; i++)
            {
                if (miarray[i] < min)
                {
                    min = miarray[i];
                }
                if (miarray[i] > max)
                {
                    max = miarray[i];
                }

            }
            Console.WriteLine($"El maximo es {max} y el minimo es {min}");
        }
        public static void ejercicio7()
        {
            int[] vector = { 1, 2, 3, 4, 5 };
            Console.WriteLine("Sin invertir: ");
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine(vector[i]);
            }
            Console.WriteLine("Invertido: ");
            vector = vector.Reverse().ToArray();
            for (int i = 0; i< 5; i++)
            {
                Console.WriteLine(vector[i]);
            }

        }


        public static void ejercicio10()
        {
            int a = 0;
            int b = 1;
            int c = 0;
            Console.WriteLine($"{a}");
            Console.WriteLine($"{b}");
            for (int i=0; i<23; i++)
            {
                c = a + b;
                Console.WriteLine($"{c}");
                a = b;
                b = c;
            } 
        }
    }
}
